<?php

class JobsController extends BaseController {

    public function __construct(Job $job)
    {
        $this->job = $job;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $jobs = Job::all();
        return Response::json( $jobs )->setCallback( Input::get('callback') );
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('jobs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all(); // form data
		  // validation rules
		  $rules = array(
		    'title'   => 'required', 
		    'link'    => 'required', 
		  ); 

		  $validator = Validator::make($input, $rules); // validate
		  // error handling
		  if($validator->fails()) {
		    if(Request::ajax()) {   // it's an ajax request                 
		      $response = array(
		         'response'  =>  'error',
		         'errors'    =>  $validator->errors()->toArray()
		      );                
		    } else { // it's an http request
		       return Redirect::intended('data')
		                  ->withInput()
		                  ->withErrors($validator);
		    }
		  } else { // validated
		     // save data
		  	return $this->job->create( Input::all() );
		  }

		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Job::find($id);
        //return View::make('jobs.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$job = Job::find($id);
        return View::make('jobs.edit')->with('job', $job);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$this->job->destroy($id);
		return '';

	}

}
