<?php

class MyJobsController extends BaseController {

	public function __construct(Job $job)
    {
        $this->job = $job;
        // RSS feed and static vars
		$this->feed = new RSSFeed();
		$this->services = $this->feed->services();
		$this->locations = $this->feed->locations();
		$this->search_terms = $this->feed->search_terms();
		$this->search_state = $this->feed->search_state();
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        // show the latest job feeds from RSS
		$feed = $this->feed;
		$services = $this->services;
		$locations = $this->locations;
		$search_terms = $this->search_terms;
		$search_state = $this->search_state;
		$is_multifeed = false;

		// defaults - set default location to 'phoenix' and search to 'web developer' with state 'AZ'
		$defaults = array(
			'location_city' => $locations[0],
			'location_state' => $search_state,
			'radius' => 50,
			'limit' => 50,
			'search' => $search_terms[0]
		);

		//exclude saved jobs from the listing
		$my_jobs = Job::all();
		$exclude = array();
		foreach($my_jobs as $mjob):
			$exclude[] = $mjob['link'];
		endforeach;

		// check request vars for location / search
		if ( Input::get('location') !== null )
			$defaults['location_city'] = Input::get('location');
		if( Input::get('search') !== null ) 
			$defaults['search'] = Input::get('search');

		$data = array(
			'body_class' => 'search',
			'terms' => $search_terms,
			'locations' => $locations,
			'services' => $services,
			'exclude' => $exclude,
			'multifeed' => $is_multifeed,
		);

		if( Input::get('service_feed') ){

			$orig_service = Input::get('service_feed');
			//if array of services found then process all into array
			if( is_array( $services[ $orig_service ] ) ){

				$service_feed = array();
				foreach( $services[ $orig_service ] as $key=>$service ){
					
					$feed_url = $feed->parse_service($service, $defaults);					
					$service_key = $orig_service . '.' . str_replace(' ','_',$key); //filename: service.industry.terms.json				
					$service_feed[] = $feed->get_feed($service_key, $defaults['search'], $feed_url);
				}
				$jobs = $service_feed;

				// for rendering nested view
				$dat = array(
					'jobs' => $jobs,
					'exclude' => $exclude
				);
				// apply data to child view
				$data['child_multi'] = View::make('myjobs.search_multi_loop')->with($dat);
				$data['jobs'] = $jobs;
				$data['multifeed'] = true;

			}else{
				$service_feed = $feed->parse_service($services[ $orig_service ], $defaults);
				$jobs = $feed->get_feed($orig_service, $defaults['search'], $service_feed);
				// for rendering nested view
				$dat = array(
					'jobs' => $jobs,
					'exclude' => $exclude
				);
				$data['jobs'] = $jobs;
				$data['child_single'] = View::make('myjobs.search_loop')->with($dat);

			}

		}else{ // Home search defaults to Indeed / Simplyhired using default values

			// parse services using default values
			$service_simplyhired = $feed->parse_service($services['simplyhired'], $defaults);
			$service_indeed = $feed->parse_service($services['indeed'], $defaults);
			//apply defaults
			$jobs = $feed->get_feed('simplyhired',$defaults['search'],$service_simplyhired);
			$indeed = $feed->get_feed('indeed',$defaults['search'],$service_indeed);

			$dat_home = array(
				'jobs' => $jobs,
				'exclude' => $exclude,
				'indeed_jobs' => $indeed
			);
			$data['jobs'] = $jobs;
			$data['indeed_jobs'] = $indeed;
			$data['child_home'] = View::make('myjobs.search_home')->with($dat_home);

		}

		return View::make('myjobs.search', $data);	
	}

	public function job_frame($url) {
		$curl = New Curl;
		return $curl->simple_get($url);
	}

	public function get_plaintext($url) {
		//replace _ with %2F before getting the html
		$pos_craigslist = stripos($url,'craigslist');
		$pos_simplyhired = stripos($url,'simplyhired');
		$pos_indeed = stripos($url,'indeed');
		$pos_jobing = stripos($url,'jobing');
		$pos_dice = stripos($url,'dice');
		$pos_careerbuilder = stripos($url,'careerbuilder');
		
		$body_class = 'body'; //default to body tag

		if( $pos_craigslist !== false ) {
			$body_class = 'section.body';
		}
		if( $pos_simplyhired !== false ) {
			$body_class = '#jobs';
		}
		if( $pos_indeed !== false ) {
			$body_class = 'td.snip';
		}
		if( $pos_jobing !== false ) {
			$body_class = 'div.job-body';
		}
		if( $pos_dice !== false ) {
			$body_class = '#detailDescription';
		}
		if( $pos_careerbuilder !== false ) {
			$body_class = 'div.main';

			// transform careerbuilder URL to mobile equivalent
			// Web: http://www.careerbuilder.com/JobSeeker/Jobs/JobDetails.aspx?siteid=RSS_PD&Job_DID=J3G6FR6C5V0502S6S3L&ipath=rss_geoip
			// Mobile: http://mobile.careerbuilder.com/seeker/job/J3G6FR6C5V0502S6S3L/?IPath=CBMTN
			preg_match('/Job_DID=(.*)&/i',$url,$matches);
			if( isset( $matches[1] ) ) {
				$url = sprintf('http://mobile.careerbuilder.com/seeker/job/%s/?IPath=CBMTN',$matches[1]);
			}
		}else{
			// prepare url for curl
			$url = urldecode( str_replace("_","%2F",str_replace("--","%3F",$url)) );
		}

		$job_frame = $this->job_frame( $url );
		
		// create Htmldom object from returned html
		$html = new Htmldom( $job_frame );
		$elem = $html->find( $body_class );

		foreach($elem as $el){
			$frame = $el->innertext;
		}
		// strip <script> and <link> tags from html
		$frame = preg_replace("#<script(.*?)>(.*?)</script>#is","", $frame);
		$frame = preg_replace("/<link[^>]+\>/i","", $frame);
		$data = array('content' => trim($frame));
		return Response::json($data);
	}

	public function my_jobs($status='new') {
		
		$jobs = Job::where('status','=',$status)->get();

		$data = array(
			'body_class' => 'my_jobs',
			'jobs' => $jobs,
			'status' => $status,
			'terms' => $this->search_terms,
			'locations' => $this->locations,
			'job_frame' => '',
			'child' => View::make('myjobs.loop')->with('jobs',$jobs)
		);		
		return View::make('myjobs.index', $data);
	}

	public function delete() {
		$id = Input::get('id');
		if( $id ){
			$job = Job::find( Input::get('id') );
			$job->delete();
			return Response::json(array('success'))->setCallback(Input::get('callback'));
		}
	}

	public function update() {
		$id = Input::get('id');
		$data = array();

		if( Input::get('status') ){
			$data['status'] = Input::get('status');
		}
		if( $id ){	
			$data['favorite'] = Input::get('favorite');
		}
		if( $id && $data ){
			$job = Job::find( $id )->update( $data );
		}
		return Response::json(Job::find( $id ));		

	}
}
