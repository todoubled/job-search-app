<?php
/*
Usage:
$feed = new RSSFeed();
$rss = $feed->get_feed('simplyhired','http://www.simplyhired.com/a/job-feed/rss/qe-web+developer/lc-phoenix/ls-AZ/mi-50/ws-50');
$feed->display_news($rss);
*/

class RSSFeed {
	// rss services include dynamic variables : search, location_city, location_state, radius, limit
	public static $services = array(
		'indeed' => 'http://rss.indeed.com/rss?q={search}&l={location_city}%2C+{location_state}&limit={limit}',
		'simplyhired' => 'http://www.simplyhired.com/a/job-feed/rss/qa-{search}/lc-{location_city}/ls-{location_state}/ws-{limit}',
		'jobing' => array(
			'Internet Ecommerce' => 'http://phoenix.jobing.com/rss/cat/521',
			'Software Development' => 'http://phoenix.jobing.com/rss/cat/523'
		),
		'dice' => 'http://rss.dice.com//system/phoenix-jobs.xml', //phoenix based jobs
		'careerbuilder' => 'http://www.careerbuilder.com/RTQ/rss20.aspx?rssid=RSS_PD&num=50&geoip=true&ddcompany=false&ddtitle=false&cat=JN008&kw={search}',
		'craigslist' => array(
			'web info design' => 'http://phoenix.craigslist.org/search/web?query={search}&format=rss',
			'internet engineering' => 'http://phoenix.craigslist.org/search/eng?query={search}&format=rss'
		),
	);
	public static $search_terms = array(
		'web developer',
		'ui developer',
		'sr web developer',
		'front end developer',
		'php developer'
	);
	public static $locations = array(
		'phoenix',
		'scottsdale',
		'tempe',
		'glendale',
		'peoria'
	);
	public static $search_state = 'AZ';

	public function services() {
		return self::$services;
	}
	public function locations() {
		return self::$locations;
	}	
	public function search_terms() {
		return self::$search_terms;
	}
	public function search_state() {
		return self::$search_state;
	}

	public function parse_service($str, $values) {
		// service RSSFeed URLs have {vars} to replace within an array {keys} with their values using urlencoding
		foreach($values as $key=>$val):
			$str = str_replace('{'.$key.'}',urlencode($val),$str);
		endforeach;

		return $str;				
	}
	public function get_feed($service, $search, $url=''){

		// get the feed
		$feed = self::cache_feed($service,$search,$url);
				
		//get the xml object
		$news = new SimpleXMLElement($feed);
		$result = array();
		if( count($news->channel->item) > 0 ){
			$items = $news->channel->item;
		}
		if( count($news->item) > 0 ){
			$items = $news->item;
		}
		foreach($items as $item){
			$result[] = array(
				'title' => (string) $item->title[0],
				'link' => (string) $item->link[0],
				'description' => trim($item->description),
				'source' => (string) $item->source[0],
				'pub_date' => (string) $item->pubDate[0]
			);
		}
		

		return $result;

	}
	public function display_news($news){
		
		print "<ul class='news-feed'>";
		foreach($news as $n){
			print '<li class="news-item"><a href="' . $n['link'] . '" target="_blank">' . $n['title'] . '</a><br>' . $n['source'] . '<p>';
			print $n['description'];
			print '</li>';
		}

		print '</ul>';

	}
	public function cache_feed($service, $search, $url) {

		//save the rss file cache
		$validCache = false;
		// replace spaces with underscores for readability in filenames
		$search = str_replace(" ","_",$search);
		$service = strtolower( str_replace(" ","_",$service) );

		$file = dirname(__FILE__) . "/json/$service.$search.json";

		if ( file_exists($file) ) {
		    $contents = file_get_contents($file);
		    $data = json_decode($contents);

		    if ( time() - $data->time < 24 * 60 * 60 ) {
		        $validCache = true;
		        $feed = $data->feed;
		    }
		}

		if (!$validCache) {
		    $feed = file_get_contents($url);
		    $data = array('feed' => $feed, 'time' => time() );
		    file_put_contents($file, json_encode($data));
		}
		return $feed;
	}
}