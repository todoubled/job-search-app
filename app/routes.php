<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'MyJobsController@index');

// my saved jobs and show by status
Route::get('my-jobs', 'MyJobsController@my_jobs');
Route::get('my-jobs/{status}', 'MyJobsController@my_jobs');

// ajax routes
Route::post('action', array('as' => 'add_job', 'uses' => 'JobsController@store'));
Route::get('/ajax/get_url/{url}', 'MyJobsController@get_plaintext');
Route::post('/ajax/delete', 'MyJobsController@delete');
Route::post('/ajax/update', 'MyJobsController@update');

Route::resource('jobs', 'JobsController');

