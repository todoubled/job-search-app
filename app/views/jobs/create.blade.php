{{ Form::open(array('route' => 'jobs.store')) }}
	<ul>
		<li>
			{{ Form::label('title', 'Title:') }}
			{{ Form::text('title') }}
		</li>

		<li>
			{{ Form::label('link', 'Link:') }}
			{{ Form::text('link') }}
		</li>

		<li>
			{{ Form::label('description', 'Description:') }}
			{{ Form::textarea('description') }}
		</li>

		<li>
			{{ Form::label('source', 'Source:') }}
			{{ Form::text('source') }}
		</li>

		<li>
			{{ Form::label('pub_date', 'Pub date:') }}
			{{ Form::text('pub_date') }}
		</li>

		<li>
			{{ Form::label('status', 'Status:') }}
			{{ Form::text('status') }}
		</li>

		<li>
			{{ Form::label('favorite', 'Favorite:') }}
			{{ Form::checkbox('favorite') }}
		</li>

		<li>
			{{ Form::submit() }}
		</li>
	</ul>
{{ Form::close() }}