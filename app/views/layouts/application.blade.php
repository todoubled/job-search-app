<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <title>Laravel4</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="A single page blog built using Backbone.js, Laravel, and Twitter Bootstrap">
 
 <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
 
 <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
 <!--[if lt IE 9]>
 <script src="{{ URL::asset('assets/js/html5shiv.js') }}"></script>
 <script src="{{ URL::asset('assets/js/respond.min.js') }}"></script>
 <![endif]-->
</head>
<body>
 
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Project name</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
 
 <div class="container" data-role="main">
  @yield('content')
 </div> <!-- /container -->
 
 <!-- Placed at the end of the document so the pages load faster -->
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> <!-- use Google CDN for jQuery to hopefully get a cached copy -->
 <script src="{{ URL::asset('/assets/js/bootstrap.min.js') }}"></script>
 <script src="{{ URL::asset('/assets/js/app.js') }}"></script>
 @yield('scripts')
</body>
</html>
