<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>My Jobs</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}">
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.6.2.min.js') }}"></script>
    </head>
    <body class="{{ $body_class }}">
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        @if ( isset($terms) || isset($locations) )
        <div id="sidebar">
            <!-- only show search menu on home / search view -->
            @if ( $body_class !== "my_jobs" )
            <p>
            <strong>Job Feeds:</strong><br>
            <select name="service_feed" class="form-control">
                <option value="">Select One</option>
                @foreach($services as $key=>$service)
                <option value="{{ urlencode($key) }}"
                    @if( Input::get('service_feed') == $key ) selected="selected"
                    @endif>{{ $key }}</option>
                @endforeach
            </select>
            </p>
            <p>
            <strong>Search Terms:</strong><br>
            <select name="search_terms" class="form-control">
                @foreach($terms as $term)
                <option value="{{ urlencode($term) }}"
                    @if( Input::get('search') == $term ) selected="selected"
                    @endif>{{ $term }}</option>
                @endforeach
            </select>
            </p>
            <p>
            <strong>Locations:</strong><br>
            <select name="locations" class="form-control">
                @foreach($locations as $location)
                <option value="{{ urlencode($location) }}"
                    @if( Input::get('location') == $location ) selected="selected"
                    @endif>{{ $location }}</option>
                @endforeach
            </select>
            </p>
            @endif
            <ul class="nav nav-pills nav-stacked">
                <li @if ( $body_class == 'search' ) class="active" @endif><a href="{{ URL::to("/"); }}"> <i class="fa fa-search"></i> Search Jobs</a></li>
                <li @if ( $body_class == 'my_jobs' ) class="active" @endif><a href="{{ URL::to("my-jobs") }}"><i class="fa fa-star"></i> My Saved Jobs</a></li>
            </ul>            
        </div>
        @endif

        <div class="container">
            @yield('content')
        </div>


        <script>    
            var BASE = '{{ URL::route("add_job"); }}';
            var BASE_URL = '{{ URL::to("/"); }}';            
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ URL::asset('assets/js/vendor/jquery-1.10.2.min.js') }}"><\/script>')</script>
        <script src="{{ URL::asset('assets/js/plugins.js') }}"></script>
        <script src="{{ URL::asset('assets/js/main.js') }}"></script>
    </body>
</html>