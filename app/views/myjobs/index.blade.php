@extends('layouts.base')

@section('content')

<div class="saved-jobs col-md-5">

<div class="filter">
	<h1><i class="fa fa-briefcase"></i> My Saved Jobs ( {{ count($jobs) }} )</h1>
	<blockquote>To mark off jobs that you have completed, simply check the box next to the title. Use the icons to the right of the title to mark your favorites, archive or delete a job listing.</blockquote>
	<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
	    @if ( $status !== 'new' )
	    	{{ $status }} <span class="caret"></span>
	    @else
	    	Filter by Status <span class="caret"></span>
	    @endif
	  </button>
	  <ul class="dropdown-menu" role="menu">
	    <li><a href="{{ URL::to('/my-jobs'); }}" data-status="new"><i class="fa fa-asterisk"></i> New</a></li>
	    <li><a href="{{ URL::to('/my-jobs/archived'); }}" data-status="archived"><i class="fa fa-archive"></i> Archived</a></li>
	    <li><a href="{{ URL::to('/my-jobs/completed'); }}" data-status="completed"><i class="fa fa-check"></i> Completed</a></li>
	  </ul>
	</div>
</div>

<br class="clearfix">

{{ $child }}
</div>

<div class="col-md-5">
    <div class="job-frame">
        {{ $job_frame }}
    </div>
</div>

@stop
