<div class="welcome">
	<ul class="list-group">
        @foreach($jobs as $job)
            <li class="list-group-item">
            	<input type="checkbox" class="job-add">
            	<a href="{{ $job['link'] }}" data-favorite="{{ $job['favorite'] }}" data-id="{{ $job['id'] }}" data-source="{{ $job['source'] }}" data-pub-date="{{ $job['pub_date'] }}" class="el">
            		{{ $job['title'] }}
            	</a><br>
                @if ( stripos("http://",$job['source']) !== false )
                <strong>{{ $job['source'] }}</strong><br>
                @endif
                <span>{{ Str::limit( strip_tags( $job['description'] ) , 250) }}</span>
                <a class="preview btn btn-primary btn-small"><i class="fa fa-share-square-o"></i> Preview</a>
                <div class="actions">
                	<a href="#" data-toggle="tooltip" title="Favorite" rel="tooltip" class="favorite @if( $job['favorite'] ) active @endif"><i class="fa fa-heart"></i></a>
                	<a href="#" data-toggle="tooltip" title="Archive" rel="tooltip" class="archive @if( $job['status']=='Archived' ) active @endif"><i class="fa fa-archive"></i></a>
                	<a href="#" data-toggle="tooltip" title="Delete" rel="tooltip" class="delete"><i class="fa fa-times"></i></a>
                </div>
            </li>
        @endforeach            
    </ul>	
</div>
