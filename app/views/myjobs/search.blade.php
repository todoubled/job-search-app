@extends('layouts.base')

@section('content')

<!-- home search : indeed / simplyhired -->
@if ( isset($indeed_jobs) )
    {{ $child_home }}
@else
    <!-- search by service -->
    @if ( $multifeed )
        {{ $child_multi }}
    @else
        {{ $child_single }}
    @endif

@endif
<div class="col-md-5">
    <div class="job-frame"></div>
</div>

@stop