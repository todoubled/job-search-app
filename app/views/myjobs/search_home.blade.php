<div class="welcome col-md-6">
        <h1><i class="fa fa-archive"></i> Indeed Jobs ( {{ count($indeed_jobs) }} )</h1>
        <ul class="list-group">
            @foreach($indeed_jobs as $job)
                @if ( !in_array($job['link'],$exclude) )
                <li class="list-group-item"><input type="checkbox" class="job-add">                     
                    <a href="{{ $job['link'] }}" data-source="{{ $job['source'] }}" data-pub-date="{{ $job['pub_date'] }}">{{ $job['title'] }}</a><br>                                       
                    <strong class="source">{{ $job['source'] }}</strong><br>
                    <span>{{ strip_tags( $job['description'] ) }}</span>
                    <strong>@if( date('m/d', strtotime( $job['pub_date'] )) == date('m/d', time()) )
                        <span class="label label-warning">NEW</span>
                    @endif {{ $job['pub_date'] }}</strong>
                </li>
                @endif
            @endforeach            
        </ul>
</div>

<div class="welcome col-md-6">
        <h1><i class="fa fa-archive"></i> SimplyHired Jobs ( {{ count($jobs) }} )</h1>
        <ul class="list-group">
            @foreach($jobs as $job)
                @if ( !in_array($job['link'],$exclude) )
                <li class="list-group-item"><input type="checkbox" class="job-add"> <a href="{{ $job['link'] }}" data-source="{{ $job['source'] }}" data-pub-date="{{ $job['pub_date'] }}">{{ $job['title'] }}</a><br>
                    <span>{{ strip_tags( $job['description'] ) }}</span>
                    <strong>@if( date('m/d', strtotime( $job['pub_date'] )) == date('m/d', time()) )
                        <span class="label label-warning">NEW</span>
                    @endif {{ $job['pub_date'] }}</strong>
                </li>
                @endif
            @endforeach            
        </ul>
</div>