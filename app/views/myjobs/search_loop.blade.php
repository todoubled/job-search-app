<div class="welcome col-md-6">
        <h1><i class="fa fa-archive"></i> {{ Input::get('service_feed') }} Jobs ( {{ count($jobs) }} )</h1>
        <ul class="list-group">
            @foreach($jobs as $job)
                @if ( !in_array($job['link'],$exclude) )
                <li class="list-group-item">
                    <input type="checkbox" class="job-add"> 
                    <a href="{{ $job['link'] }}" class="el" data-source="{{ $job['source'] }}" data-pub-date="{{ $job['pub_date'] }}">{{ $job['title'] }}</a><br>
                    <span>{{ strip_tags( $job['description'] ) }}</span>
                    <a class="preview btn btn-primary btn-small"><i class="fa fa-share-square-o"></i> Preview</a>
                    <strong>@if( date('m/d', strtotime( $job['pub_date'] )) == date('m/d', time()) )
                        <span class="label label-warning">NEW</span>
                    @endif {{ $job['pub_date'] }}</strong>
                </li>
                @endif
            @endforeach            
        </ul>
</div>