@foreach( $jobs as $job )

<div class="welcome col-md-6">
        <h1><i class="fa fa-archive"></i> {{ Input::get('service_feed') }} Jobs ( {{ count($job) }} )</h1>

        <ul class="list-group">
            @foreach($job as $jo)
                @if ( !in_array($jo['link'],$exclude) )
                <li class="list-group-item"><input type="checkbox" class="job-add"> <a href="{{ $jo['link'] }}" data-source="{{ $jo['source'] }}" data-pub-date="{{ $jo['pub_date'] }}">{{ $jo['title'] }}</a><br>
                    <span>{{ strip_tags( $jo['description'] ) }}</span>
                    <strong>@if( date('m/d', strtotime( $jo['pub_date'] )) == date('m/d', time()) )
                        <span class="label label-warning">NEW</span>
                    @endif {{ $jo['pub_date'] }}</strong>
                </li>
                @endif
            @endforeach            

        </ul>
</div>
@endforeach