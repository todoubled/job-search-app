$( document ).ready(function() {
  
  // on search, if checkbox is clicked then save to myjobs
  $("body.search .job-add").click(function(e){
    var el = $(this);
    var el_data = el.parent().find('a');
    var el_parent = el.parent();

    var data = {
        title: el_data.text(),
        link: el_data.attr('href'),
        source: el_parent.find('strong.source').html(),
        description: el_parent.find('span').html(),
        pub_date: el_data.data('pub-date'),
        favorite: 0,
        status: 'new'
    };

    $.post(BASE, data, function(data){ console.log(data); }).fail(function(jqxhr, textStatus, error) {
     var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    }).success(function(){
        el_parent.slideUp("slow");
    }); 

    //console.log(data);
  });
  
  // actions for my jobs

  // delete job
  $("body.my_jobs a.delete").click(function(e){
        e.preventDefault();
        var agreed = confirm("Are you sure you want to delete?");
        if( agreed ){
            var el = $(this).parent().parent().find('a');
            var el_parent = $(this).parent().parent();
            var id = el.data('id');
            var del_url = BASE_URL + '/ajax/delete';
            $.post(del_url, { id: id }, function(data){ console.log(data); }).fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            }).success(function(){
                el_parent.slideUp("slow");
            });
        }
  });
  $("body.my_jobs a.archive, body.my_jobs a.favorite").click(function(e){
        e.preventDefault();
        var icon = $(this).find('i');
        var el = $(this).parent().parent().find('a');
        var el_parent = $(this).parent().parent();
        var id = el.data('id');
        var fav = el.data('favorite');

        if( $(this).hasClass('favorite') ){
            // using data-favorite to toggle state
            if( !fav ){
                var data = { id: id, favorite: 1 };
                el.data('favorite',1);
            }else{
                var data = { id: id, favorite: 0 };
                el.data('favorite',0);
            }
        }else{ //archive
            var data = { id: id, status: 'Archived' };
        }
        //console.log(data);
        $.post( BASE_URL + '/ajax/update', data, 
            function(data){ 
                //console.log(data);
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            }).success(function(data){
                console.log(data);
                if( data.status == "Archived" ){
                    el_parent.slideUp("slow");
                }else{
                    if( data.favorite == "1" ){
                        icon.animate({ opacity: 1 });
                    }else{
                        icon.animate({ opacity: 0.5 });
                    }
                }
            });
  });

$("body.search a.preview, body.my_jobs a.preview").click(function(e){
    e.preventDefault();
    var el = $(this).parent().find("a.el");
    var el_parent = $(this).parent();
    var href = encodeURIComponent( el.attr('href') );

    var re = new RegExp('%2F', 'g');
    var rep = new RegExp('%3F', 'g');
    href = href.replace(re, '_');
    href = href.replace(rep, '--');

    if( href ){ 
            $.get( BASE_URL + '/ajax/get_url/' + href, 
            function(data){ 
                //console.log(data);
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            }).success(function(data){
                $("div.job-frame").html( data.content ).animate({ top: window.top.scrollY }).attr('style','position:absolute');
                $("ul.list-group li").removeClass("highlight");
                el_parent.addClass("highlight");
            });
    }
});
$("body.my_jobs input.job-add").click(function(e){
        var el = $(this).parent().find('a');
        var el_parent = $(this).parent();
        var id = el.data('id');
        var data = { id: id, status: 'Completed' };

        $.post(BASE_URL + '/ajax/update', data, 
            function(data){ 
                //console.log(data);
                
            })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            }).success(function(){
                el_parent.slideUp("slow");
            });
  });

  $('a[rel=tooltip]').tooltip();
  //sidebar events
  $("#sidebar select[name='service_feed']").change(function(e){
    var service = $("#sidebar select[name='service_feed']").val();
    var search = $("#sidebar select[name='search_terms']").val() ? $("#sidebar select[name='search_terms']").val() : null;
    var loc = $("#sidebar select[name='locations']").val() ? $("#sidebar select[name='locations']").val() : null;
    window.location.replace('?service_feed='+$(this).val()+'&location='+loc+'&search='+search);
  });  
  $("#sidebar select[name='search_terms']").change(function(e){
    var loc = $("#sidebar select[name='locations']").val();
    window.location.replace('?search='+$(this).val()+'&location='+loc);
  });
  $("#sidebar select[name='locations']").change(function(e){
    var search = $("#sidebar select[name='search_terms']").val();
    window.location.replace('?location='+$(this).val()+'&search='+search);
  });
});