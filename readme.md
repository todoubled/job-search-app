## Job Search App using the Laravel PHP Framework

[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework)

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### License

This software and the Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Install

1) Download the source and run : composer update

2) Update the database (app/config/database.php) and run : php artisan migrate

3) RSSFeed class has some static variables to update relating to your job search. 

- Update the file: /app/customlib/RSSFeed.php

- The $services array contains the RSS feeds to various job sites, feel free to add your own. You can do a single feed or multiple feeds, if you add multiple feeds per service limit it to two. With a multi-feed service you won't be able to preview the job postings.

- The $search_terms array contains all of your common job titles. Update with your own set of terms and put your most common search on the first array item so it becomes the default search.

- The $locations array contains the cities you are searching within.

4) There are default search variables you can modify. Open the file at app/controllers/MyJobsControllers.php - line #30 - modify the $defaults array in the index() method. These are the default search values the application will use.

5) When searching jobs the app will save the data into json files in the /app/customlib/json/ folder. It should automatically create the folder for you but if not, create the 'json' folder with write permissions.
